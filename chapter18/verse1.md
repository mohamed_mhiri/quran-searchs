## chapter 18 verse 1

الْحَمْدُ لِلَّـهِ الَّذِي أَنزَلَ عَلَىٰ عَبْدِهِ الْكِتَابَ وَلَمْ يَجْعَل لَّهُ عِوَجًا ۜ

[All] praise is [due] to Allah, who has sent down upon His Servant the Book and has not made therein any deviance.

Louange à Allah qui a fait descendre sur Son serviteur (Muhammad), le Livre, et n'y a point introduit de tortuosité (ambiguité)!
