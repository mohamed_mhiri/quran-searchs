## chapter 18 verse 6

فَلَعَلَّكَ بَاخِعٌ نَّفْسَكَ عَلَىٰ آثَارِهِمْ إِن لَّمْ يُؤْمِنُوا بِهَـٰذَا الْحَدِيثِ أَسَفًا

Yet perchance, if they believe not in this tiding, thou wilt consume thyself, following after them, of grief.

Tu vas peut-être te consumer de chagrin parce qu'ils se détournent de toi et ne croient pas en ce discours!