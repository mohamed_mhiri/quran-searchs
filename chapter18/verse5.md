## chapter 18 verse 5

مَّا لَهُم بِهِ مِنْ عِلْمٍ وَلَا لِآبَائِهِمْ ۚ كَبُرَتْ كَلِمَةً تَخْرُجُ مِنْ أَفْوَاهِهِمْ ۚ إِن يَقُولُونَ إِلَّا كَذِبًا

They have no knowledge of it, nor had their fathers. Grave is the word that comes out of their mouths; they speak not except a lie.

Ni eux ni leurs ancêtres n'en savent rien. Quelle monstrueuse parole que celle qui sort de leurs bouches! Ce qu'ils disent n'est que mensonge.
