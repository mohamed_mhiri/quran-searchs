## chapter 29 verse 2

أَحَسِبَ النَّاسُ أَن يُتْرَكُوا أَن يَقُولُوا آمَنَّا وَهُمْ لَا يُفْتَنُونَ

Do the people think that they will be left to say, "We believe" and they will not be tried?

Est-ce que les gens pensent qu'on les laissera dire: «Nous croyons!» sans les éprouver?
