## chapter 29 verse 4

أَمْ حَسِبَ الَّذِينَ يَعْمَلُونَ السَّيِّئَاتِ أَن يَسْبِقُونَا ۚ سَاءَ مَا يَحْكُمُونَ

Or do those who do evil deeds think they can outrun Us? Evil is what they judge.

Ou bien ceux qui commettent des méfaits, comptent-ils pouvoir Nous échapper? Comme leur jugement est mauvais!
