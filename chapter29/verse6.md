## chapter 29 verse 6

وَمَن جَاهَدَ فَإِنَّمَا يُجَاهِدُ لِنَفْسِهِ ۚ إِنَّ اللَّـهَ لَغَنِيٌّ عَنِ الْعَالَمِينَ

And whoever strives only strives for [the benefit of] himself. Indeed, Allah is free from need of the worlds.

Et quiconque lutte, ne lutte que pour lui-même, car Allah peut Se passer de tout l'univers.
