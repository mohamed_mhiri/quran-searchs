## chapter 29 verse 5

مَن كَانَ يَرْجُو لِقَاءَ اللَّـهِ فَإِنَّ أَجَلَ اللَّـهِ لَآتٍ ۚ وَهُوَ السَّمِيعُ الْعَلِيمُ

Whoever should hope for the meeting with Allah - indeed, the term decreed by Allah is coming. And He is the Hearing, the Knowing.

Celui qui espère rencontrer Allah, le terme fixé par Allah va certainement venir. Et c'est Lui l'Audient, l'Omniscient.
