## Chapter 25 verse 7

 وَقَالُوا مَالِ هَـٰذَا الرَّسُولِ يَأْكُلُ الطَّعَامَ وَيَمْشِي فِي الْأَسْوَاقِ ۙ لَوْلَا أُنزِلَ إِلَيْهِ مَلَكٌ فَيَكُونَ مَعَهُ نَذِيرًا

 And they say, "What is this messenger that eats food and walks in the markets? Why was there not sent down to him an angel so he would be with him a warner?

 Et ils disent: «Qu'est-ce donc que ce Messager qui mange de la nourriture et circule dans les marchés? Que n'a-t-on fait descendre vers lui un Ange qui eût été avertisseur en sa compagnie?
