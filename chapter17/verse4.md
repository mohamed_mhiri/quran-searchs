## chapter 17 verse 4

وَقَضَيْنَا إِلَىٰ بَنِي إِسْرَائِيلَ فِي الْكِتَابِ لَتُفْسِدُنَّ فِي الْأَرْضِ مَرَّتَيْنِ وَلَتَعْلُنَّ عُلُوًّا كَبِيرًا

And We decreed for the Children of Israel in the Book: 'You shall do corruption in the earth twice, and you shall ascend exceeding high.'

Nous avions décrété pour les Enfants d'Israël, (et annoncé) dans le Livre: «Par deux fois vous sèmerez la corruption sur terre et vous allez transgresser d'une façon excessive».
