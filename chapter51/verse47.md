## chapter 51 verse 47

وَالسَّمَاءَ بَنَيْنَاهَا بِأَيْدٍ وَإِنَّا لَمُوسِعُونَ

And the heaven We constructed with strength, and indeed, We are [its] expander.

Le ciel, Nous l'avons construit par Notre puissance: et Nous l'étendons [constamment] dans l'immensité.
