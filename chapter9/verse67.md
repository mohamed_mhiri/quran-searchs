## chapter 9 verse 67

الْمُنَافِقُونَ وَالْمُنَافِقَاتُ بَعْضُهُم مِّن بَعْضٍ ۚ يَأْمُرُونَ بِالْمُنكَرِ وَيَنْهَوْنَ عَنِ الْمَعْرُوفِ وَيَقْبِضُونَ أَيْدِيَهُمْ ۚ نَسُوا اللَّـهَ فَنَسِيَهُمْ ۗ إِنَّ الْمُنَافِقِينَ هُمُ الْفَاسِقُونَ

The hypocrite men and hypocrite women are of one another. They enjoin what is wrong and forbid what is right and close their hands. They have forgotten Allah, so He has forgotten them [accordingly]. Indeed, the hypocrites - it is they who are the defiantly disobedient.

Les hypocrites, hommes et femmes, appartiennent les uns aux autres. Ils commandent le blâmable, interdisent le convenable, et replient leurs mains (d'avarice). Ils ont oublié Allah et Il les a alors oubliés. En vérité, les hypocrites sont les pervers.


نسوا الله فنسيهم، فإن معناه: تركوا الله أن يطيعوه ويتبعوا أمره, فتركهم الله من توفيقه وهدايته ورحمته.

فالنسيان يفيد أيضاالترك عمدا

They have forgotten Allah, so he has forgotten them: They have chosen to disobey Allah and not to follow his commands, so they missed Allah's reconcile, guidance and mercy.

Forgetfulness in arabic also means the deliberate abandonment.

Ils ont oublié Allah et Il les a alors oubliés:Ils ont choisi de désobéir à Allah et de ne pas suivre ses commandements, alors ils ont raté la réconciliation, la guidance et la miséricorde d'Allah.

L'oubli en arabe signifie aussi l'abandon délibéré.
