## chapter 1 verse 5

إِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ

It is You we worship and You we ask for help.

C'est Toi [Seul] que nous adorons, et c'est Toi [Seul] dont nous implorons secours.
